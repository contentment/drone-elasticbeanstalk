drone-elasticbeanstalk [![Build Status](http://ci.contentment.io/api/badges/contentment/drone-elasticbeanstalk/status.svg)](http://ci.contentment.io/contentment/drone-elasticbeanstalk)
======

A plugin to deploy your project in Amazon ElasticBeanstalk written in Python and using AWS APK. 

* **region** - bucket region (`us-east-1`, `eu-west-1`, etc)
* **access_key** - amazon key
* **secret_key** - amazon secret key
* **app_name** - elasticbeanstalk application name
* **env_name** - elasticbeanstalk environment name
* **source_bundle_key** - deployment dest file on S3
* **source_bundle_bucket** - deployment dest bucket on S3
* **version_label** - elasticbeanstalk version label
* **env_dir** - directory to define environment variables to update

The following is a sample drone-elasticbeanstalk configuration in your 
.drone.yml file:

```yaml
deploy:
  elasticbeanstalk:
    image: 'contentment/drone-elasticbeanstalk'
    region: 'eu-west-1'
    access_key: $${AWS_ACCESS_KEY}
    secret_key: $${AWS_SECRET_KEY}
    app_name: 'appname'
    env_name: 'envname'
    source_bundle_key: 'path/Dockerrun.aws.json'
    source_bundle_bucket: 'bucketname'
    version_label: 'versionlabel'
    env_dir:'.env.d':
```
