#!/usr/bin/env python
"""
A plugin to deploy your project in Amazon ElasticBeanstalk
"""

import datetime
from os import listdir
from os.path import isdir, isfile, join
import time

import boto3
import drone
import requests


def get_env_vars(filepath):

    # Checking if env_dir exists.
    if not isdir(filepath):
        print ("> Your environment directory `env_dir` is missing...KO")
        exit(2)

    # Get files with environment variables values to update.
    files = [f for f in listdir(filepath) if isfile(join(filepath, f))]
    options = []

    if len(files) > 0:
        # Format environment variables to update in OptionsSettings.
        print("> Getting environment variables to update : ")
        for filename in files :
            with open(join(filepath, filename), 'r') as content:
                value = content.read();
                print("   - " + filename + " = " + value)
                options.append(
                        {
                            #'ResourceName': 'string',
                            'Namespace': "aws:elasticbeanstalk:application:environment",
                            'OptionName': filename,
                            'Value': value
                        }
                )
        print("  => OK")

    return options


def main():
    """
    The main entrypoint for the plugin.
    """
    # Retrives plugin input from stdin/argv, parses the JSON, returns a dict.
    payload = drone.plugin.get_input()

    #build = payload["build"]
    repo = payload["repo"]
    workspace = payload["workspace"]
    vargs = payload["vargs"]

    # Check if ElasticBeanstalk application and environment are define.
    if "app_name" not in vargs:
        vargs["app_name"] = repo["name"]

    if "env_name" not in vargs:
        print("Please provide an environment name")
        exit(1)

    # Check if AWS Credentials are defined.
    if "access_key" not in vargs:
        print("Please provide an access key id")
        exit(1)

    if "secret_key" not in vargs:
        print("Please provide a secret access key")
        exit(1)

    # Define default value for region in case value not defined.
    if "region" not in vargs:
        print("Please provide a region")
        exit(1)

    # Check in version label is defined.
    if "version_label" not in vargs:
        print("Please provide a version label")
        exit(1)

    # Check if Source Bundle is defined (key and bucket).
    if "source_bundle_bucket" not in vargs:
        print("Please provide a bucket name")
        exit(1)

    if "source_bundle_key" not in vargs:
        print("Please provide a bucket key")
        exit(1)

    # Connecting to AWS.
    print("> Connecting to AWS...", end="")
    session = boto3.session.Session(
            aws_access_key_id = vargs["access_key"],
            aws_secret_access_key = vargs["secret_key"],
            region_name = vargs["region"]
    )
    eb = session.client("elasticbeanstalk")
    print("OK")

    # Delete version (to be able to override it and allow rebuild in CI).
    print("> Deleting existing Version in ElasticBeanstalk...", end="")
    response = eb.delete_application_version(
            ApplicationName = vargs["app_name"],
            VersionLabel = vargs["version_label"],
            DeleteSourceBundle = False
    )

    if response["ResponseMetadata"]["HTTPStatusCode"] != 200:
        print("KO")
    else:
        print("OK")

    # Create new version.
    print("> Creating new Version in ElasticBeanstalk...", end="")
    response = eb.create_application_version(
            ApplicationName = vargs["app_name"],
            VersionLabel = vargs["version_label"],
            SourceBundle = {
                'S3Bucket': vargs["source_bundle_bucket"],
                'S3Key': vargs["source_bundle_key"]
            },
            #Process = False,
            AutoCreateApplication = False
    )

    if response["ResponseMetadata"]["HTTPStatusCode"] != 200:
        print("KO")
        print("Response = " + response)
        exit(10)
    else:
        print("OK")

    options = []
    if "env_dir" in vargs:
        options = get_env_vars(join(workspace["path"], vargs["env_dir"]))

    # Waiting to be ready to deploy...
    while True:
        time.sleep(30)
        response = eb.describe_environments(
                ApplicationName = vargs["app_name"],
                EnvironmentNames = [vargs["env_name"]],
                IncludeDeleted = False
        )

        if response["Environments"][0]["Status"] == "Ready":
            break

    # Process new version deployment.
    print("> Updating environment to new Version...", end="")
    response = eb.update_environment(
            # ApplicationName = vargs["app_name"],
            EnvironmentName = vargs["env_name"],
            VersionLabel = vargs["version_label"],
            OptionSettings = options
    )

    if response["ResponseMetadata"]["HTTPStatusCode"] != 200:
        print("KO")
        print("Response = " + response)
        exit(20)

    # Waiting while AWS EB processing...
    while True:
        time.sleep(30)
        response = eb.describe_environments(
                ApplicationName = vargs["app_name"],
                EnvironmentNames = [vargs["env_name"]],
                IncludeDeleted = False
        )

        if response["Environments"][0]["Status"] != "Updating":
            break

    # Checking version label to know if it's deployed.
    if response["ResponseMetadata"]["HTTPStatusCode"] != 200 \
            or response["Environments"][0]["VersionLabel"] != vargs["version_label"]:
        print("KO")
        exit(30)
    else:
        print("OK")

    exit(0)


if __name__ == "__main__":
    main()
