##
#
#	Docker image used to build drone-elasticbeanstalk plugin.
#
#     docker build --rm=true -t contentment/drone-elasticbeanstalk .
#
# @author:         	Geoffrey BERGERET <geoffrey@contentment.io>
# @docker_version: 	1.8.3
# @since:			19/01/2016
# @version:        	2.0
##

FROM gliderlabs/alpine:3.2
MAINTAINER Geoffrey BERGERET <geoffrey@contentment.io>

RUN apk-install python3
RUN mkdir -p /opt/drone
COPY requirements.txt /opt/drone/
WORKDIR /opt/drone
RUN pip3 install -r requirements.txt
COPY plugin /opt/drone/

ENTRYPOINT ["python3", "main.py"]
